#ifndef ORGANISERHOME_H
#define ORGANISERHOME_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class OrganiserHome; }
QT_END_NAMESPACE

class OrganiserHome : public QMainWindow
{
    Q_OBJECT

public:
    OrganiserHome(QWidget *parent = nullptr);
    ~OrganiserHome();

private:
    Ui::OrganiserHome *ui;
};
#endif // ORGANISERHOME_H

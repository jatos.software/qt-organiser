#include "organiserhome.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    OrganiserHome w;
    w.show();
    return a.exec();
}
